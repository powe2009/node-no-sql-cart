const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');

const defaultUserId = '5d9a4c0e62daf00cec1fa062';
const User = require('./models/user');
const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');
const errorController = require('./controllers/error');
const mongoConnect = require('./util/database').mongoConnect;

const app = express();

app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use((req, res, next) => {
  User.findOne(defaultUserId)
    .then(user => {
      req.user = new User(user.name, user.email, user._id, user.cart)
      next()
    })
    .catch( error => {
      console.log("TCL: error", error)
    })
});  

app.use('/admin', adminRoutes);
app.use(shopRoutes);

app.use(errorController.get404);

mongoConnect( db => {
  app.listen(3000);
});


