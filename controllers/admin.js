const Product = require('../models/product');
const User = require('../models/user');

exports.getAddProduct = (req, res, next) => {
  res.render('admin/edit-product', {
    pageTitle: 'Add Product',
    path: '/admin/add-product',
    editing: false
  });
};

exports.postAddProduct = (req, res, next) => {
  const title = req.body.title;
  const imageUrl = req.body.imageUrl;
  const price = req.body.price;
  const desc = req.body.desc;
  const product = new Product(title, price, imageUrl, desc, req.user._id, null);
  
  product.save()
    .then(result => {
      res.redirect('/admin/products');
    })
    .catch(err => {
      console.log(err);
    });
};

exports.getProducts = (req, res, next) => {
  Product.findAll()
    .then(products => {
      res.render('admin/products', {
        prods: products,
        pageTitle: 'Admin Products',
        path: '/admin/products'
      });
    })
    .catch(err => console.log(err));
};

exports.getEditProduct = (req, res, next) => {
  const editMode = req.query.edit;
  if (!editMode) {
    return res.redirect('/');
  }
  
  const prodId = req.params.productId;
  Product.findOne(prodId)
    .then(product => {
      res.render('admin/edit-product', {
        pageTitle: 'Edit Product',
        path: '/admin/edit-product',
        editing: editMode,
        product: product
      });
    })
    .catch(err => console.log(err));
};

exports.postEditProduct = (req, res, next) => {
  const prodId = req.body.productId;
  const updatedTitle = req.body.title;
  const updatedPrice = req.body.price;
  const updatedImageUrl = req.body.imageUrl;
  const updatedDesc = req.body.desc;
  
  const product = new Product(updatedTitle, updatedPrice, updatedImageUrl, updatedDesc, req.user._id, prodId);
  
  product.save()
    .then(result => {
      res.redirect('/admin/products');    
    })
    .catch(error => {
      console.log("TCL: exports.postEditProduct -> error", error);
    })
 };

exports.postDeleteProduct = (req, res, next) => {
  const prodId = req.body.productId;
   
  Product.deleteOne(prodId)
    .then(result => {
      console.log('DESTROYED PRODUCT');
      res.redirect('/admin/products');
    })
    .catch(err => console.log(err));
};

exports.addUser = (req, res, next) => {
  res.render('admin/edit-user', {
    pageTitle: 'Add User',
    path: '/admin/users',
    editing: false
  });
};

exports.getUser = (req, res, next) => {
  const userId = req.params.userId;
  User.findOne(userId)
    .then(user => {
      res.render('admin/edit-user', {
        pageTitle: 'Update User',
        path: '/admin/users',
        editing: false,
        user: user,
        editing: true
      });
    })
};

exports.postUser = (req, res, next) => {
  const user = new User(req.body.name, req.body.email, req.body.userId);
  console.log("TCL: exports.postUser -> req.body.userId", req.body.userId)
  user.save()
    .then(result => {
      res.redirect('/admin/users');    
    })
    .catch(error => {
      console.log("TCL: exports.postUser -> error", error);
    })
};

exports.getUsers = (req, res, next) => {
  User.findAll()
    .then(users => {
      res.render('admin/users', {
        users: users,
        pageTitle: 'Admin Users',
        path: '/admin/users',
      });
    })
    .catch(err => console.log(err));
};

exports.deleteUser = (req, res, next) => {
  const userId = req.body.userId;
  User.deleteOne(userId)
    .then(result => {
      res.redirect('/admin/users');
    })
    .catch(err => console.log(err));
};