const Product = require('../models/product');
const User = require('../models/user');

exports.getIndex = (req, res, next) => {
  Product.findAll()
    .then(products => {
      res.render('shop/index', {
        prods: products,
        pageTitle: 'Shop',
        path: '/'
      });
    })
    .catch(err => {
      console.log(err);
    });
};

exports.getProducts = (req, res, next) => {
  Product.findAll()
    .then(products => {
      res.render('shop/product-list', {
        prods: products,
        pageTitle: 'All Products',
        path: '/products'
      });
    })
    .catch(err => {
      console.log(err);
    });
};

exports.getProduct = (req, res, next) => {
  const prodId = req.params.productId;
  Product.findOne(prodId)
    .then(product => {
      res.render('shop/product-detail', {
        product: product,
        pageTitle: product.title,
        path: '/products'
      });
    })
    .catch(err => console.log(err));
};
   
exports.getCart = (req, res, next ) =>  {
  req.user.getCartItems()
    .then(products => {
      res.render('shop/cart', {
        products: products,
        pageTitle: 'Your Cart',
        path: '/cart'
      })
    })
    .catch(error => {
      console.log("TCL: exports.getCart -> error", error)
    })
};

exports.postCart = (req, res, next) => {
  req.user.updateCart(req.body.productId, req.user.cart)
    .then(result => {
      res.redirect('/products');
    })
    .catch(error => {
      console.log("TCL: exports.postCart -> error", error)
    })
}

exports.deleteCartItem = (req, res, next) => {
  req.user.deleteCartItem(req.body.productId)
    .then(result => {
      res.redirect('/cart');
    })
    .catch(error => {
      console.log("TCL: exports.deleteCartItem -> error", error)
    })
}

exports.postOrder = (req, res, next) => {
  req.user.postOrder()
    .then(result => {
      res.redirect('/cart');
    })
    .catch(error => {
      console.log("TCL: exports.postOrder -> error", error)
    })
}

exports.getOrders = (req, res, next) => {
  req.user.getOrders()
    .then(orders => {
      res.render('shop/orders', {
        orders: orders,
        pageTitle: 'Your Orders',
        path: '/orders'
      })
    })
    .catch(error => {
      console.log("TCL: exports.getOrders -> error ", error )
    })

}





