const getDb = require('../util/database').getDb;
const mongodb = require('mongodb');

class Product {
  constructor (title, price, imageUrl, desc, userId, id) {
    this.title = title;
    this.price = price;
    this.imageUrl = imageUrl;
    this.desc = desc;
    this.userId = userId;
    this.id = id;
  }

  save() {
    const db = getDb();
    if (this.id) {
    return db.collection('products')
      .updateOne({_id: new mongodb.ObjectId(this.id)}, 
        {$set: {title: this.title, price: this.price, imageUrl: this.imageUrl, desc: this.desc}
      })
      .then (result => {
        return result;
      })
    } else {
      return db.collection('products').insertOne(
        {title: this.title, price: this.price, imageUrl: this.imageUrl, desc: this.desc, userId: this.userId}
      )
      .then (result => {
        return result;
      })
    }
  }

  static deleteOne(prodId) {
    const db = getDb();
    let id = new mongodb.ObjectId(prodId);
    
    return db.collection('products')
      .deleteOne({_id: id})
      .then (result => {
        return result;
      })
  }

  static findAll() {
    const db = getDb();
    return db.collection('products')
      .find()
      .toArray()
      .then (products => {
        
        return products
      })
      .catch(error => {
        console.log("TCL: Product -> save -> error", error)
      })
  }

  static findOne(prodId) {
    const db = getDb();
    return db.collection('products')
      .find({_id: new mongodb.ObjectId(prodId)})
      .next()
      .then(product => {
        return product;
      })
      .catch(error => {
        console.log("TCL: Product -> save -> error", error)
      })
  }

}

module.exports = Product;

