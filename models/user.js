const getDb = require('../util/database').getDb;
const mongodb = require('mongodb');

const objectId = mongodb.ObjectId;

class User {
  constructor (name, email, id, cart) {
    this.name = name;
    this.email = email;
    this.id = id;
    this.cart = cart;
  }

  getCartItems() {
    const db = getDb();
    const prodIds = this.cart.items
      .map(item => {
        return new objectId(item.prodId)
      });
    
    // console.log("TCL: User -> getCartItems -> this.id.toString()", typeof (this.id.toString()))
    // console.log("TCL: User -> getCartItems -> this.id", typeof this.id)
    // console.log("TCL: User -> getCartItems -> new mongodb.this.id", typeof (new mongodb.ObjectId(this.id)));
          
    return db.collection('products')
      .find({ _id: { $in: prodIds } })
      .toArray()
      .then(products => {
        return products.map(p => {
          return {
            ...p,   
            qty: this.cart.items.find(item => {
                  return item.prodId == p._id;
                }).qty
          }
        })
      })
      .catch(error => {
        console.log("TCL: User -> getCartItems -> error", error)
      })
  }

  deleteCartItem(productId) {
    const updatedCartItems = this.cart.items.filter(item => {
      return item.prodId.toString() !== productId.toString();
    });
    const db = getDb();

    return db
      .collection('users')
      .updateOne(
        { _id: new objectId(this.id) },
        { $set: { cart: {items: updatedCartItems} } }
      );
  }

  updateCart(prodId, cart) {
    const db = getDb(); 
    let updatedCart = {items: [{}]};

    if (!cart || !cart.items) {
      updatedCart.items[0] = {prodId: prodId, qty: 1};
    } else {
      // do not match on type as in "==="
      updatedCart = {...cart};
      let index = cart.items.findIndex(
          item => item.prodId == prodId
      );
      if (index > -1) {
        let qty = +cart.items[index].qty;
        updatedCart.items[index] = {prodId: prodId, qty: ++qty};
      } else {
        updatedCart.items.push( {prodId: prodId, qty: 1});
      }
    }

    return db.collection('users')
      .updateOne(
          {_id: new objectId(this.id)}, 
          {$set: {cart: updatedCart}})
      .then(result => {
      })
      .catch(result => {
        console.log("TCL: User -> updateCart -> result", result)
      })
  }

  postOrder () {
    const db = getDb();
    const order = {userId: new objectId(this.id)};

    return this.getCartItems()
      .then(cartItems => {
        order.items = cartItems.map(item => {
          return  {id: item._id, title: item.title, price: item.price, qty: item.qty}
        });

        return db.collection('orders')
          .insertOne(order)
          .then(result => {
            this.cart = {items: [{}]}
            return db.collection('users')
              .updateOne(
                {_id: new objectId(this.id)},
                {$set: {cart: {items: []}}}
            )
            .then(result => {
            })
            .catch(result => {
              console.log("TCL: User -> postOrder -> result", result)
            })
          })     
          .catch(error => {
            console.log("TCL: User -> postOrder -> error", error)
          })     
       })
  }

  getOrders () {
    const db = getDb();
    return db.collection('orders')
      .find({ userId: new objectId(this.id) } )
      .toArray()
      .then( orders => {
         return orders
       })
      .catch( error => {
         console.log("TCL: User -> getOrders -> error", error)
      })
  }


  /**
   * User maintenance
   * 
   */
  static findOne(userId) {
    const db = getDb();
    return db.collection('users')
      .find({_id: new mongodb.ObjectId(userId)})
      .next()
      .then(user => {
        return user;
      })
      .catch(error => {
        console.log("TCL: User -> save -> error", error)
      })
  }

  static deleteOne(userId) {
    const db = getDb();
    let id = new mongodb.ObjectId(userId);
    
    return db.collection('users')
      .deleteOne({_id: id})
      .then (result => {
        return result;
      })
  }

  save() {
    const db = getDb();
    if (this.id) {
       return db.collection('users')
        .updateOne({_id: new mongodb.ObjectId(this.id)}, {$set: { name: this.name, email: this.email }})
        .then (result => {
          return result;
      })
    } else {
      return db.collection('users').insertOne({ name: this.name, email: this.email })
        .then (result => {
          return result;
      })
    }
  }

  static findAll() {
    const db = getDb();
    return db.collection('users')
      .find()
      .toArray()
      .then (users => {
        return users
      })
      .catch(error => {
        console.log("TCL: User -> findAll -> error", error)
      })
  }

}

module.exports = User;

